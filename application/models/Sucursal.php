<?php
  /**
   *
   */
  class Sucursal extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }//fin de la funcion constructor
    //insertar nuevos hospitales
    function insertar($datos){//$datos: es un array- conjunto de datos
      $respuesta=$this->db->insert("sucursal",$datos);//insert nos deja insertar registros permite dos parametros: la tabla y un conjunto de datos informativos como nombre, etc
      return $respuesta;
    }//fin funcion insertar
    //consulta de datos
    function consultarTodos(){
      $sucursales=$this->db->get("sucursal");
      if($sucursales->num_rows()>0) {
        return $sucursales->result();
      } else {
        return false;
      }//fin del else
    }//fin de la funcion consulta
    //Eliminacion de hospital por id
    function eliminar($id){
      $this->db->where("id_pc",$id);
      return $this->db->delete("sucursal");
    }//fin de la funcion eliminar
    //Consulta de un solo hospital
    function obtenerPorId($id){
      $this->db->where("id_pc",$id);
      $sucursal=$this->db->get("sucursal");
      if ($sucursal->num_rows()>0) {
        return $sucursal->row();
      } else {
        return false;
      }
    }
    //actualizar HOSPITAL
    function actualizar($id,$datos){
      $this->db->where("id_pc",$id);
      return $this->db->update("sucursal",$datos);
    }
  }//fin de la clase Hospital
 ?>
