<?php
  /**
   *
   */
  class Cajero extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }//fin de la funcion constructor
    //insertar nuevos hospitales
    function insertar($datos){//$datos: es un array- conjunto de datos
      $respuesta=$this->db->insert("cajero",$datos);//insert nos deja insertar registros permite dos parametros: la tabla y un conjunto de datos informativos como nombre, etc
      return $respuesta;
    }//fin funcion insertar
    //consulta de datos
    function consultarTodos(){
      $cajeros=$this->db->get("cajero");
      if($cajeros->num_rows()>0) {
        return $cajeros->result();
      } else {
        return false;
      }//fin del else
    }//fin de la funcion consulta
    //Eliminacion de hospital por id
    function eliminar($id){
      $this->db->where("id_pc",$id);
      return $this->db->delete("cajero");
    }//fin de la funcion eliminar
    //Consulta de un solo hospital
    function obtenerPorId($id){
      $this->db->where("id_pc",$id);
      $cajero=$this->db->get("cajero");
      if ($cajero->num_rows()>0) {
        return $cajero->row();
      } else {
        return false;
      }
    }
    //actualizar HOSPITAL
    function actualizar($id,$datos){
      $this->db->where("id_pc",$id);
      return $this->db->update("cajero",$datos);
    }
  }//fin de la clase Hospital
 ?>
