<?php
class Footer_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    // Insertar datos del footer
    function insertar($datos)
    {
        $respuesta = $this->db->insert("footer", $datos);
        return $respuesta;
    }

    // Consultar datos del footer
    function obtenerInformacion()
    {
        $footer = $this->db->get("footer");
        if ($footer->num_rows() > 0) {
            return $footer->row_array();
        } else {
            return false;
        }
    }

    // Actualizar datos del footer
    function actualizarInformacion($id,$datos)
    {
        $this->db->where("id_pc",);
        $this->db->update("footer", $datos);
        return $this->db->update("footer",$datos);
    }
}
?>
