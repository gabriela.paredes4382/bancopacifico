<?php
class Footer extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Footer_model');// Carga la librería de validación de formularios
    }
    public function editar()
    {
        $data['footerEditar'] = $this->Footer_model->obtenerInformacion();
        $this->load->view("header");
        $this->load->view('footer/editar_footer', $data);
    }

    public function actualizar()
    {
        $id_pc = $this->input->post("id_pc");
        // Si la validación es exitosa, actualiza la información del footer en la base de datos
        $datos = array(
            'direccion' => $this->input->post('direccion'),
            'telefono' => $this->input->post('telefono'),
            'email' => $this->input->post('email')
        );
        $this->Footer_model->actualizarInformacion($id_pc,$datos);
        $this->session->set_flashdata('confirmacion', 'La información del footer se actualizó correctamente.'); // Mensaje de éxito usando flashdata
        redirect('dashboards/index'); // Redirige de nuevo a la página de edición
    }
}
?>
