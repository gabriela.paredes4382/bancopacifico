<?php
  class Corresponsales extends CI_Controller
  {
    function __construct()
    {
      parent::__construct();
      $this->load->model("Corresponsal");
      //desabilitando errores y advertencias
      //error_reporting(0);
    }//fin de la funcionconstructor
    //renderizacion de la vista index de hospitales
    public function index(){
      $data["listadoCorresponsales"]=$this->Corresponsal->consultarTodos();//array asociativo:data, tiene una posicion: listadoHospitales
      $this->load->view("header");
      $this->load->view("corresponsales/index",$data);
      $this->load->view("footer");
    }//fin de la funcion index
    //eliminacion de hospitales recibiendo el id por get
    public function borrar($id_pc){
      $this->session->set_flashdata("eliminar","Esta seguro que desea eliminar el dato seleccionado");
      $this->Corresponsal->eliminar($id_pc);
      $this->session->set_flashdata("confirmacion","Corresponsal Eliminado Exitosamente");
      redirect("corresponsales/index");
    }//fin de la funcion borrar
    //renderizacion de formulario nuevo hospital
    public function nuevo(){
      $this->load->view("header");
      $this->load->view("corresponsales/nuevo");
      $this->load->view("footer");
    }
    //capturando datos y e insertando en Hospital
    public function guardarCorresponsal(){
      /*INICIO PROCESO DE SUBIDA DE ARCHIVO*/
      $config['upload_path']=APPPATH.'../uploads/corresponsales/'; //ruta de subida de archivos
      $config['allowed_types']='jpeg|jpg|png';//tipo de archivos permitidos
      $config['max_size']=5*1024;//definir el peso maximo de subida (5MB)
      $nombre_aleatorio="corresponsal_".time()*rand(100,10000);//creando un nombre aleatorio
      $config['file_name']=$nombre_aleatorio;//asignando el nombre al archivo subido
      $this->load->library('upload',$config);//cargando la libreria UPLOAD
      if($this->upload->do_upload("fotografia_pc")){ //intentando subir el archivo
         $dataArchivoSubido=$this->upload->data();//capturando informacion del archivo subido
         $nombre_archivo_subido=$dataArchivoSubido["file_name"];//obteniendo el nombre del archivo
      }else{
        $nombre_archivo_subido="";//Cuando no se sube el archivo el nombre queda VACIO
      }
      $datosNuevoCorresponsal=array(
      "nombre_pc"=>$this->input->post("nombre_pc"),
      "pais_pc"=>$this->input->post("pais_pc"),
      "provincia_pc"=>$this->input->post("provincia_pc"),
      "ciudad_pc"=>$this->input->post("ciudad_pc"),
      "direccion_pc"=>$this->input->post("direccion_pc"),
      "tipo_pc"=>$this->input->post("tipo_pc"),
      "horario_pc"=>$this->input->post("horario_pc"),
      "servicio_pc"=>$this->input->post("servicio_pc"),
      "latitud_pc"=>$this->input->post("latitud_pc"),
      "longitud_pc"=>$this->input->post("longitud_pc"),
      "fotografia_pc"=>$nombre_archivo_subido);
      $this->Corresponsal->insertar($datosNuevoCorresponsal);
      $this->session->set_flashdata("confirmacion","Corresponsal Guardado Exitosamente"); //flash _sata crea una session de tipo flash, aparece y desaparece
      enviarEmail("bryan.figueroa1979@utc.edu.ec","Creacion","<h5> Se creo el corresponsal: </h5>".$datosNuevoCorresponsal['nombre_pc']);
      redirect('corresponsales/index');
    }
    //Renderizar el formulario de edicion
    public function editar($id){
      $data["corresponsalEditar"]=$this->Corresponsal->obtenerPorId($id);
      $this->load->view("header");
      $this->load->view("corresponsales/editar",$data);
      $this->load->view("footer");
    }
    //actualizar Hospital
    public function actualizarCorresponsal(){
      $id_pc=$this->input->post("id_pc");
      $datosCorresponsal=array(
        "nombre_pc"=>$this->input->post("nombre_pc"),
        "pais_pc"=>$this->input->post("pais_pc"),
        "provincia_pc"=>$this->input->post("provincia_pc"),
        "ciudad_pc"=>$this->input->post("ciudad_pc"),
        "direccion_pc"=>$this->input->post("direccion_pc"),
        "tipo_pc"=>$this->input->post("tipo_pc"),
        "horario_pc"=>$this->input->post("horario_pc"),
        "servicio_pc"=>$this->input->post("servicio_pc"),
        "latitud_pc"=>$this->input->post("latitud_pc"),
        "longitud_pc"=>$this->input->post("longitud_pc")
      );
      $this->Corresponsal->actualizar($id_pc,$datosCorresponsal);
      $this->session->set_flashdata("confirmacion",
      "Corresponsal actualizado exitosamente");
      redirect('corresponsales/index');
    }
  }//fin de la clase
 ?>
