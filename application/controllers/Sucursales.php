<?php
  class Sucursales extends CI_Controller
  {
    function __construct()
    {
      parent::__construct();
      $this->load->model("Sucursal");
      //desabilitando errores y advertencias
      //error_reporting(0);
    }//fin de la funcionconstructor
    //renderizacion de la vista index de hospitales
    public function index(){
      $data["listadoSucursales"]=$this->Sucursal->consultarTodos();//array asociativo:data, tiene una posicion: listadoHospitales
      $this->load->view("header");
      $this->load->view("sucursales/index",$data);
      $this->load->view("footer");
    }//fin de la funcion index
    //eliminacion de hospitales recibiendo el id por get
    public function borrar($id_pc){
      $this->Sucursal->eliminar($id_pc);
      $this->session->set_flashdata("confirmacion","Sucursal Eliminada Exitosamente");
      redirect("sucursales/index");
    }//fin de la funcion borrar
    //renderizacion de formulario nuevo hospital
    public function nuevo(){
      $this->load->view("header");
      $this->load->view("sucursales/nuevo");
      $this->load->view("footer");
    }
    //capturando datos y e insertando en Hospital
    public function guardarSucursal(){
      /*INICIO PROCESO DE SUBIDA DE ARCHIVO*/
      $config['upload_path']=APPPATH.'../uploads/sucursales/'; //ruta de subida de archivos
      $config['allowed_types']='jpeg|jpg|png';//tipo de archivos permitidos
      $config['max_size']=5*1024;//definir el peso maximo de subida (5MB)
      $nombre_aleatorio="sucursal_".time()*rand(100,10000);//creando un nombre aleatorio
      $config['file_name']=$nombre_aleatorio;//asignando el nombre al archivo subido
      $this->load->library('upload',$config);//cargando la libreria UPLOAD
      if($this->upload->do_upload("fotografia_pc")){ //intentando subir el archivo
         $dataArchivoSubido=$this->upload->data();//capturando informacion del archivo subido
         $nombre_archivo_subido=$dataArchivoSubido["file_name"];//obteniendo el nombre del archivo
      }else{
        $nombre_archivo_subido="";//Cuando no se sube el archivo el nombre queda VACIO
      }
      $datosNuevoSucursal=array(
      "nombre_pc"=>$this->input->post("nombre_pc"),
      "pais_pc"=>$this->input->post("pais_pc"),
      "provincia_pc"=>$this->input->post("provincia_pc"),
      "ciudad_pc"=>$this->input->post("ciudad_pc"),
      "direccion_pc"=>$this->input->post("direccion_pc"),
      "horarioAtencion_pc"=>$this->input->post("horarioAtencion_pc"),
      "contacto_pc"=>$this->input->post("contacto_pc"),
      "latitud_pc"=>$this->input->post("latitud_pc"),
      "longitud_pc"=>$this->input->post("longitud_pc"),
      "fotografia_pc"=>$nombre_archivo_subido);
      $this->Sucursal->insertar($datosNuevoSucursal);
      $this->session->set_flashdata("confirmacion","Sucursal Guardada Exitosamente"); //flash _sata crea una session de tipo flash, aparece y desaparece
      enviarEmail("bryan.figueroa1979@utc.edu.ec","Creacion","<h5> Se creo la sucursal: </h5>".$datosNuevoSucursal['nombre_pc']);
      redirect('sucursales/index');
    }
    //Renderizar el formulario de edicion
    public function editar($id){
      $data["sucursalEditar"]=$this->Sucursal->obtenerPorId($id);
      $this->load->view("header");
      $this->load->view("sucursales/editar",$data);
      $this->load->view("footer");
    }
    //actualizar Hospital
    public function actualizarSucursal(){
      $id_pc=$this->input->post("id_pc");
      $datosSucursal=array(
        "nombre_pc"=>$this->input->post("nombre_pc"),
        "pais_pc"=>$this->input->post("pais_pc"),
        "provincia_pc"=>$this->input->post("provincia_pc"),
        "ciudad_pc"=>$this->input->post("ciudad_pc"),
        "direccion_pc"=>$this->input->post("direccion_pc"),
        "horarioAtencion_pc"=>$this->input->post("horarioAtencion_pc"),
        "contacto_pc"=>$this->input->post("contacto_pc"),
        "latitud_pc"=>$this->input->post("latitud_pc"),
        "longitud_pc"=>$this->input->post("longitud_pc")
      );
      $this->Sucursal->actualizar($id_pc,$datosSucursal);
      $this->session->set_flashdata("confirmacion",
      "Sucursal actualizada exitosamente");
      redirect('sucursales/index');
    }
  }//fin de la clase
 ?>
