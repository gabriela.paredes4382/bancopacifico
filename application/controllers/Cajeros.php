<?php
  class Cajeros extends CI_Controller
  {
    function __construct()
    {
      parent::__construct();
      $this->load->model("Cajero");
      //desabilitando errores y advertencias
      //error_reporting(0);
    }//fin de la funcionconstructor
    //renderizacion de la vista index de hospitales
    public function index(){
      $data["listadoCajeros"]=$this->Cajero->consultarTodos();//array asociativo:data, tiene una posicion: listadoHospitales
      $this->load->view("header");
      $this->load->view("cajeros/index",$data);
      $this->load->view("footer");
    }//fin de la funcion index
    //eliminacion de hospitales recibiendo el id por get
    public function borrar($id_pc){
      $this->Cajero->eliminar($id_pc);
      $this->session->set_flashdata("confirmacion","Cajero Eliminado Exitosamente");
      redirect("cajeros/index");
    }//fin de la funcion borrar
    //renderizacion de formulario nuevo hospital
    public function nuevo(){
      $this->load->view("header");
      $this->load->view("cajeros/nuevo");
      $this->load->view("footer");
    }
    //capturando datos y e insertando en Hospital
    public function guardarCajero(){
      /*INICIO PROCESO DE SUBIDA DE ARCHIVO*/
      $config['upload_path']=APPPATH.'../uploads/cajeros/'; //ruta de subida de archivos
      $config['allowed_types']='jpeg|jpg|png';//tipo de archivos permitidos
      $config['max_size']=5*1024;//definir el peso maximo de subida (5MB)
      $nombre_aleatorio="cajero_".time()*rand(100,10000);//creando un nombre aleatorio
      $config['file_name']=$nombre_aleatorio;//asignando el nombre al archivo subido
      $this->load->library('upload',$config);//cargando la libreria UPLOAD
      if($this->upload->do_upload("foto_pc")){ //intentando subir el archivo
         $dataArchivoSubido=$this->upload->data();//capturando informacion del archivo subido
         $nombre_archivo_subido=$dataArchivoSubido["file_name"];//obteniendo el nombre del archivo
      }else{
        $nombre_archivo_subido="";//Cuando no se sube el archivo el nombre queda VACIO
      }
      $datosNuevoCajero=array(
      "pais_pc"=>$this->input->post("pais_pc"),
      "provincia_pc"=>$this->input->post("provincia_pc"),
      "ciudad_pc"=>$this->input->post("ciudad_pc"),"direccion_pc"=>$this->input->post("direccion_pc"),
      "tipo_pc"=>$this->input->post("tipo_pc"),
      "estado_pc"=>$this->input->post("estado_pc"),
      "latitud_pc"=>$this->input->post("latitud_pc"),
      "longitud_pc"=>$this->input->post("longitud_pc"),
      "foto_pc"=>$nombre_archivo_subido);
      $this->Cajero->insertar($datosNuevoCajero);
      $this->session->set_flashdata("confirmacion","Cajero Guardado Exitosamente"); //flash _sata crea una session de tipo flash, aparece y desaparece
      enviarEmail("bryan.figueroa1979@utc.edu.ec","Creacion","<h5> Se creo el cajero: </h5>".$datosNuevoCajero['tipo_pc']);
      redirect('cajeros/index');
    }
    //Renderizar el formulario de edicion
    public function editar($id){
      $data["cajeroEditar"]=$this->Cajero->obtenerPorId($id);
      $this->load->view("header");
      $this->load->view("cajeros/editar",$data);
      $this->load->view("footer");
    }
    //actualizar Hospital
    public function actualizarCajero(){
      $id_pc=$this->input->post("id_pc");
      $datosCajero=array(
        "pais_pc"=>$this->input->post("pais_pc"),
        "provincia_pc"=>$this->input->post("provincia_pc"),
        "ciudad_pc"=>$this->input->post("ciudad_pc"),
        "direccion_pc"=>$this->input->post("direccion_pc"),
        "tipo_pc"=>$this->input->post("tipo_pc"),
        "estado_pc"=>$this->input->post("estado_pc"),
        "latitud_pc"=>$this->input->post("latitud_pc"),
        "longitud_pc"=>$this->input->post("longitud_pc")
      );
      $this->Cajero->actualizar($id_pc,$datosCajero);
      $this->session->set_flashdata("confirmacion",
      "Cajero actualizado exitosamente");
      redirect('cajeros/index');
    }
  }//fin de la clase
 ?>
