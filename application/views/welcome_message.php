<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- basic -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- mobile metas -->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="viewport" content="initial-scale=1, maximum-scale=1">
      <!-- site metas -->
      <title>Giftshop</title>
      <meta name="keywords" content="">
      <meta name="description" content="">
      <meta name="author" content="">
      <!-- bootstrap css -->
      <link rel="stylesheet" type="text/css" href="assets/css1/bootstrap.min.css">
      <!-- style css -->
      <link rel="stylesheet" type="text/css" href="assets/css1/style.css">
      <!-- Responsive-->
      <link rel="stylesheet" href="assets/css1/responsive.css">
      <!-- fevicon -->
      <link rel="icon" href="assets/images1/fevicon.png" type="image/gif" />
      <!-- Scrollbar Custom CSS -->
      <link rel="stylesheet" href="assets/css1/jquery.mCustomScrollbar.min.css">
      <!-- Tweaks for older IEs-->
      <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
      <!-- owl stylesheets -->
      <link rel="stylesheet" href="assets/css1/owl.carousel.min.css">
      <link rel="stylesheet" href="assets/css1/owl.theme.default.min.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
   </head>
   <body>
      <!--header section start -->
      <!--header section start -->
      <div class="header_section">
         <div class="container-fluid">
            <div class="row">
               <div class="col-sm-6 col-lg-2">
                  <span  style="font-size:33px;cursor:pointer; color: #ffffff;" onclick="openNav()"><img src="assets/images1/toggle-menu.png" class="toggle_menu"></span>
               </div>
               <div class="col-sm-6 col-lg-2">
                  <div class="logo"><a href="index.html"><img src="https://images.squarespace-cdn.com/content/v1/55d9fb0ee4b0dfd798034243/1542823840795-3HOMCTR3YSBE7PG1UKHM/bdpwhite.png"></a></div>
               </div>
               <div class="col-sm-8">
                  <div class="menu_text">
                     <ul>
                        <li class="active"><a href="<?php echo site_url('dashboards/index'); ?>">Personas</a></li>
                        <li class="active"><a href="<?php echo site_url('welcome_message.php'); ?>">Personas</a></li>
                        <li><a href="about.html">Empresas</a></li>
                        <li><a href="gifts.html">PacifiCard</a></li>
                        <li><a href="shop.html">Shop</a></li>
                        <li><a href="contact.html">Contact Us</a></li>
                        <li><a href="#"><img src="assets/images1/search-icon.png"></a></li>
                        <div id="myNav" class="overlay">
                           <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                           <div class="overlay-content">
                              <a href="index.html">Personas</a>
                              <a href="about.html">Empresas</a>
                              <a href="gifts.html">PacifiCard</a>
                           </div>
                        </div>
                  </div>
                  </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- header section end -->
      <!-- banner section start -->
      <!-- <div class="banner_main">
         <div id="my_Controls" class="carousel-slide" data-ride="carousel">
            <div class="carousel-inner">
               <div class="carousel-item active">
                  <div class="banner_section">
                     <div class="row">
                        <div class="col-md-6">
                           <div class="banner_taital">
                              <h2 class="our_text">Our Best</h2>
                              <h1 class="gifts_text">Gifts Shop</h1>
                              <p class="standerd_text">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested</p>
                              <div class="red_bt"><a href="#">Read More</a></div>
                           </div>
                        </div>
                        <div class="col-md-6 ram">
                           <div class="right_img"><img src="assets/images1/img-1.png"></div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="carousel-item">
                  <div class="banner_section">
                     <div class="row">
                        <div class="col-md-6">
                           <div class="banner_taital">
                              <h2 class="our_text">Our Best</h2>
                              <h1 class="gifts_text">Gifts Shop</h1>
                              <p class="standerd_text">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested</p>
                              <div class="red_bt"><a href="#">Read More</a></div>
                           </div>
                        </div>
                        <div class="col-md-6 ram">
                           <div class="right_img"><img src="assets/images1/img-1.png"></div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="carousel-item">
                  <div class="banner_section">
                     <div class="row">
                        <div class="col-md-6">
                           <div class="banner_taital">
                              <h2 class="our_text">Our Best</h2>
                              <h1 class="gifts_text">Gifts Shop</h1>
                              <p class="standerd_text">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested</p>
                              <div class="red_bt"><a href="#">Read More</a></div>
                           </div>
                        </div>
                        <div class="col-md-6 ram">
                           <div class="right_img"><img src="assets/images1/img-1.png"></div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <a class="carousel-control-prev" href="#my_Controls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#my_Controls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
            </a>
         </div>
      </div> -->
      <!-- banner section end -->
      <div class="background_bg">
         <!-- about section start -->
         <div class="container">
            <div class="about_section layout_padding">
               <div class="row">
                 <div class="col-md-6">
                    <div class=""><img src="https://www.bancodelpacifico.com/BancoPacifico/media/layout-images/bancamovil-2021/6.gif"></div>
                 </div>
                  <div class="col-md-6">
                     <div class="about_taital">
                        <h2 style="color: #0069c0;">Banca Móvil</h2>
                        <p class="true_text">¡Tenemos nuevas funcionalidades! Accede a tus cuentas, tarjetas, realiza inversiones, pagos y transferencias desde tu celular.</p>
                        <div class="btn btn-primary"><a href="#">Ingresa Aquí</a></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="container">
            <div class="about_section layout_padding">
               <div class="row">
                  <div class="col-md-6">
                     <div class="about_taital">
                        <h2 style="color: #0069c0;">Depósitos a Plazo</h2>
                        <p class="true_text">Invertir en el futuro es invertir en la vida. Incrementa tu rentabilidad según el monto y plazo que establezcas. Inversión a partir de 30 días y desde $200.</p>
                        <div class="btn btn-primary"><a href="#">Solicítalo</a></div>
                     </div>
                  </div>
                  <div class="col-md-2">
                  </div>
                  <div class="col-md-4">
                     <div class=""><img src="https://quantumoptica.com/wp-content/uploads/2020/05/QUANTUM-Razones-para-comprar-gafas-en-linea-SUBTITULO-300x300.png"></div>
                  </div>
               </div>
            </div>
         </div>
         <div class="container">
            <div class="about_section layout_padding">
               <div class="row">
                 <div class="col-md-6">
                    <div class=""><img src="https://img.freepik.com/fotos-premium/retrato-familia-feliz-nueva-casa-techo-carton-seguridad-seguros-e-inversion-futura-bienes-raices-mudanza-cajas-felicidad-mama-papa-ninos-casa-hipoteca-propiedad_590464-181200.jpg"></div>
                 </div>
                  <div class="col-md-6">
                     <div class="about_taital">
                        <h2 style="color: #0069c0;">Hipoteca Pacífico</h2>
                        <p class="true_text">Con el 80% de financiamiento, Hipoteca Pacífico es el crédito perfecto para empezar a vivir en tu casa soñada.</p>
                        <div class="btn btn-primary"><a href="#">Ingresa Aquí</a></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- about section end -->
         <!-- gifts section start -->
         <!-- gifts section end -->
         <!-- shop section start -->
         <!-- shop section end -->
      </div>
      <!-- contact section start -->
      <div class="contact_section layout_padding">
         <div class="contact_bg">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-sm-4 padding_0">
                     <div class="form_main">
                          <div class="dummy_text"><img src="assets/images1/map-icon.png"><span class="address_text">Address : Indoamérica, Ambato</span></div>
                          <div class="call_text"><img src="assets/images1/call-icon.png"><span class="address_text">Phone :  +(593)98 5608 531</span></div>
                          <div class="call_text"><img src="assets/images1/email-icon.png"><span class="address_text">Email : pacifico@gmail.com</span></div>
                       </div>
                  </div>
                  <div class="col-sm-8 padding_0">
                     <div class="map-responsive">
                        <iframe src="https://www.google.com/maps/embed/v1/place?key=AIzaSyA0s1a7phLN0iaD6-UE7m4qP-z21pH0eSc&q=Eiffel+Tower+Paris+France" width="600" height="560" frameborder="0" style="border:0; width: 100%;" allowfullscreen></iframe>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- contact section end -->
      <!-- section footer start -->

      </div>
      <!-- section footer end -->
      <!-- copyright section start -->
      <!-- Javascript files-->
      <script src="assets/js1/jquery.min.js"></script>
      <script src="assets/js1/popper.min.js"></script>
      <script src="assets/js1/bootstrap.bundle.min.js"></script>
      <script src="assets/js1/jquery-3.0.0.min.js"></script>
      <script src="assets/js1/plugin.js"></script>
      <!-- sidebar -->
      <script src="assets/js1/jquery.mCustomScrollbar.concat.min.js"></script>
      <script src="assets/js1/custom.js"></script>
      <!-- javascript -->
      <script src="assets/js1/owl.carousel.js"></script>
      <script src="https:cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
      <script>
         $(document).ready(function(){
         $(".fancybox").fancybox({
            openEffect: "none",
            closeEffect: "none"
            });

            $(".zoom").hover(function(){

            $(this).addClass('transition');
            }, function(){

            $(this).removeClass('transition');
            });
            });

      </script>
      <script>
         function openNav() {
         document.getElementById("myNav").style.width = "100%";
         }

         function closeNav() {
         document.getElementById("myNav").style.width = "0%";
         }
      </script>
   </body>
</html>
