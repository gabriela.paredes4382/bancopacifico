<h1><i class="mdi mdi-credit-card menu-icon"></i>  Sucursales</h1><br>
<div class="row">
  <div class="col-md-12 text-end">
    <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
      <i class="fa fa-eye"></i> VER MAPA
    </button>
    <a href="<?php echo site_url('sucursales/nuevo') ?>" class="btn btn-outline-success"> <i class="fa fa-plus-circle"></i>  Agregar Corresponsales</a>
    <br><br>
  </div>
</div>
<?php if ($listadoSucursales): ?>
<div class="table-responsive pt-3">
  <table class="table table-bordered">
    <thead>
      <tr class="table-info">
        <th>ID</th>
        <th>NOMBRE</th>
        <th>PAIS</th>
        <th>PROVINCIA</th>
        <th>CIUDAD</th>
        <th>DIRECCION</th>
        <th>HORARIO ATENCIÓN</th>
        <th>CONTACTO</th>
        <th>LATITUD</th>
        <th>LONGITUD</th>
        <th>FOTO</th>
        <th>ACCIONES</th>
      </tr>
    </thead>
    <tbody >
      <?php foreach ($listadoSucursales as $sucursal): ?>
        <tr class="table-warning">
          <td><?php echo $sucursal->id_pc; ?></td>
          <td><?php echo $sucursal->nombre_pc; ?></td>
          <td><?php echo $sucursal->pais_pc; ?></td>
          <td><?php echo $sucursal->provincia_pc; ?></td>
          <td><?php echo $sucursal->ciudad_pc; ?></td>
          <td><?php echo $sucursal->direccion_pc; ?></td>
          <td><?php echo $sucursal->horarioAtencion_pc; ?></td>
          <td><?php echo $sucursal->contacto_pc; ?></td>
          <td><?php echo $sucursal->latitud_pc; ?></td>
          <td><?php echo $sucursal->longitud_pc; ?></td>
          <td>
            <?php if ($sucursal->fotografia_pc!=""): ?>
              <img src="<?php echo base_url('uploads/sucursales/').$sucursal->fotografia_pc; ?>"
              height="100px" alt="">
            <?php else: ?>
              N/A
            <?php endif; ?>
          </td>
          <td>
            <a href="<?php echo site_url('sucursales/editar/').$sucursal->id_pc; ?>" class="btn btn-warning" title="Editar"> <i class="fa fa-pen"></i> </a>
            <a href="<?php echo site_url('sucursales/borrar/').$sucursal->id_pc?>" class="btn btn-danger" title="Eliminar"> <i class="fa-solid fa fa-trash"></i> </a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
</div>
  <!-- Button trigger modal -->
  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel"> <i class="fa fa-eye"></i> Mapa de Sucursales Registrados</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div id="reporteMapa" style="height:300px; width:100%; border:2px solid black"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>

  <script>
    function initMap() {
      var coordenadaCentral = new google.maps.LatLng(-0.9331171090790464, -78.62036815621722);
      var miMapa = new google.maps.Map(
        document.getElementById('reporteMapa'), {
          center: coordenadaCentral,
          zoom: 8,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
      );
      <?php foreach ($listadoSucursales as $sucursal): ?>
      var coordenadaTemporal = new google.maps.LatLng(
        <?php echo $sucursal->latitud_pc; ?>,
        <?php echo $sucursal->longitud_pc; ?>
      );
      var marcador = new google.maps.Marker({
        position: coordenadaTemporal,
        map: miMapa,
        title: '<?php echo $sucursal->nombre_pc; ?>'
      });
      <?php endforeach; ?>
    }
  </script>



<?php else: ?>
  <div class="alert alert-danger">
    NO SE ENCONTRO SUCURSALES REGISTRADOS
  </div>
<?php endif; ?>
