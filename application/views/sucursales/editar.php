<h1>EDITAR HOSPITAL</h1>
<form class="" action="<?php echo site_url('sucursales/actualizarSucursal') ?>" method="post" enctype="multipart/form-data">
  <input type="hidden" name="id_pc" id="id_pc" value="<?php echo $sucursalEditar->id_pc; ?>">
  <label for=""> <b>NOMBRE:</b> </label>
  <input type="text" name="nombre_pc" id="nombre_pc" value="<?php echo $sucursalEditar->nombre_pc; ?>" class="form-control" placeholder="Ingrese el NOmbre" required>
  <br>
  <label for=""> <b>PAIS:</b> </label>
  <input type="text" name="pais_pc" id="pais_pc" value="<?php echo $sucursalEditar->pais_pc; ?>" class="form-control" placeholder="Ingrese el País" required>
  <br>
  <label for=""> <b>PROVINCIA:</b> </label>
  <input type="text" name="provincia_pc" id="provincia_pc" value="<?php echo $sucursalEditar->provincia_pc; ?>" class="form-control" placeholder="Ingrese la provincia" required>
  <br>
  <label for=""> <b>CIUDAD:</b> </label>
  <input type="text" name="ciudad_pc" id="ciudad_pc" value="<?php echo $sucursalEditar->ciudad_pc; ?>" class="form-control" placeholder="Ingrese la ciudad" required>
  <br>
  <label for=""> <b>DIRECCIÓN:</b> </label>
  <textarea name="direccion_pc" id="direccion_pc" rows="8" cols="80" class="form-control" placeholder="Ingrese la direccion" required> <?php echo $sucursalEditar->direccion_pc; ?> </textarea>
  <br>
  <label for=""> <b>HORARIO DE ATENCIÓN:</b> </label>
  <input type="text" name="horarioAtencion_pc" id="horarioAtencion_pc" value="<?php echo $sucursalEditar->horarioAtencion_pc; ?>" class="form-control" placeholder="Ingrese el horario de atención" required>
  <br>
  <label for=""> <b>CONTACTO:</b> </label>
  <input type="text" name="contacto_pc" id="contacto_pc" value="<?php echo $sucursalEditar->contacto_pc; ?>" class="form-control" placeholder="Ingrese el servicio" required>
  <br>
  <label for=""> <b>FOTO ACTUAL:</b> </label>
	<br>
	<?php if (isset($sucursalEditar->fotografia_pc) && !empty($sucursalEditar->fotografia_pc)) : ?>
		<img src="<?php echo base_url('uploads/sucursales/' . $sucursalEditar->fotografia_pc); ?>" alt="" style="max-width: 300px;">
	<?php else: ?>
		N/A
	<?php endif; ?>
	<br>
  <!-- Input para cargar nueva foto si es necesario -->
	<label for=""> <b>NUEVA FOTOGRAFÍA: </b> </label>
	<input type="file" name="fotografia_pc" id="fotografia_pc" accept="image/*" class="form-control" placeholder="Ingrese una foografía">
	<br>
  <div class="row">
      <div class="col-md-6">
        <br>
        <label for="">
        <b>LATITUD:</b>
      </label>
      <input type="number" name="latitud_pc" id="latitud_pc"
			value="<?php echo $sucursalEditar->latitud_pc; ?>"
      placeholder="Ingrese la latitud" class="form-control" readonly>

      </div>
      <div class="col-md-6">
        <br>
        <label for="">
        <b>Longitud:</b>
      </label>
      <input type="number" name="longitud_pc" id="longitud_pc"
			value="<?php echo $sucursalEditar->longitud_pc; ?>"
      placeholder="Ingrese la longitud..." class="form-control" readonly>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div id="mapa" style="height: 250px; whidth:100%; border:1px solid black;">

      </div>
      </div>

    </div>
    <br>
    <br>
    <div class="row">
      <div class="col-md-12 text-center">
        <button type="submit" name="button" class="btn btn-warning"><i class="fa fa-pen"></i> &nbsp Actualizar</button> &nbsp &nbsp
        <a href="<?php echo site_url('sucursales/index'); ?>" class="btn btn-danger"> <i class="fa fa-xmark fa-spin"></i> &nbsp Cancelar</a>

      </div>

    </div>
</form>
<br>
<br>
<script>
  function initMap() {
    var latitudInicial = parseFloat('<?php echo $sucursalEditar->latitud_pc; ?>');
    var longitudInicial = parseFloat('<?php echo $sucursalEditar->longitud_pc; ?>');
    var coordenadaInicial = new google.maps.LatLng(latitudInicial, longitudInicial);

    var miMapa = new google.maps.Map(document.getElementById('mapa'), {
      center: coordenadaInicial,
      zoom: 10,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var marcador = new google.maps.Marker({
      position: coordenadaInicial,
      map: miMapa,
      title: 'Seleccione la ubicación',
      draggable: true
    });

    google.maps.event.addListener(marcador, 'dragend', function(event) {
      var latitud = this.getPosition().lat();
      var longitud = this.getPosition().lng();
      document.getElementById('latitud_pc').value = latitud;
      document.getElementById('longitud_pc').value = longitud;
    });
  }
</script>
