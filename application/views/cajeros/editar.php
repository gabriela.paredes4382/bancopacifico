<h1>EDITAR HOSPITAL</h1>
<form class="" action="<?php echo site_url('cajeros/actualizarCajero') ?>" method="post" enctype="multipart/form-data">
  <label for=""> <b>PAIS:</b> </label>
  <input type="text" name="pais_pc" id="pais_pc" value="<?php echo $cajeroEditar->pais_pc; ?>" class="form-control" placeholder="Ingrese el País" required>
  <br>
  <label for=""> <b>PROVINCIA:</b> </label>
  <input type="text" name="provincia_pc" id="provincia_pc" value="<?php echo $cajeroEditar->provincia_pc; ?>" class="form-control" placeholder="Ingrese la provincia" required>
  <br>
  <label for=""> <b>CIUDAD:</b> </label>
  <input type="text" name="ciudad_pc" id="ciudad_pc" value="<?php echo $cajeroEditar->ciudad_pc; ?>" class="form-control" placeholder="Ingrese la ciudad" required>
  <br>
  <label for=""> <b>DIRECCIÓN:</b> </label>
  <textarea name="direccion_pc" id="direccion_pc" rows="8" cols="80" class="form-control" placeholder="Ingrese la direccion" required> <?php echo $cajeroEditar->direccion_pc; ?> </textarea>
  <br>
  <label for=""> <b>TIPO:</b> </label>
  <input type="text" name="tipo_pc" id="tipo_pc" value="<?php echo $cajeroEditar->tipo_pc; ?>" class="form-control" placeholder="Ingrese el tipo" required>
  <br>
  <label for=""> <b>ESTADO:</b> </label>
  <input type="text" name="estado_pc" id="estado_pc" value="<?php echo $cajeroEditar->estado_pc; ?>" class="form-control" placeholder="Ingrese el estado" required>
  <br>
  <label for=""> <b>FOTO ACTUAL:</b> </label>
	<br>
	<?php if (isset($cajeroEditar->foto_pc) && !empty($cajeroEditar->foto_pc)) : ?>
		<img src="<?php echo base_url('uploads/cajeros/' . $cajeroEditar->foto_pc); ?>" alt="" style="max-width: 300px;">
	<?php else: ?>
		N/A
	<?php endif; ?>
	<br>
  <div class="row">
      <div class="col-md-6">
        <br>
        <label for="">
        <b>LATITUD:</b>
      </label>
      <input type="number" name="latitud_pc" id="latitud_pc"
			value="<?php echo $cajeroEditar->latitud_pc; ?>"
      placeholder="Ingrese la latitud" class="form-control" readonly>

      </div>
      <div class="col-md-6">
        <br>
        <label for="">
        <b>Longitud:</b>
      </label>
      <input type="number" name="longitud_pc" id="longitud_pc"
			value="<?php echo $cajeroEditar->longitud_pc; ?>"
      placeholder="Ingrese la longitud..." class="form-control" readonly>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div id="mapa" style="height: 250px; whidth:100%; border:1px solid black;">

      </div>
      </div>

    </div>
    <br>
    <br>
    <div class="row">
      <div class="col-md-12 text-center">
        <button type="submit" name="button" class="btn btn-warning"><i class="fa fa-pen"></i> &nbsp Actualizar</button> &nbsp &nbsp
        <a href="<?php echo site_url('cajeros/index'); ?>" class="btn btn-danger"> <i class="fa fa-xmark fa-spin"></i> &nbsp Cancelar</a>

      </div>

    </div>
</form>

<br>
<br>
