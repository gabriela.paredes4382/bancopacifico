<h1><i class="mdi mdi-credit-card menu-icon"></i>  Cajeros</h1><br>
<div class="row">
  <div class="col-md-12 text-end">
    <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
      <i class="fa fa-eye"></i> VER MAPA
    </button>
    <a href="<?php echo site_url('cajeros/nuevo') ?>" class="btn btn-outline-success"> <i class="fa fa-plus-circle"></i>  Agregar Cajero</a>
    <br><br>
  </div>
</div>
<?php if ($listadoCajeros): ?>
<div class="table-responsive pt-3">
  <table class="table table-bordered">
    <thead>
      <tr class="table-info">
        <th>ID</th>
        <th>PAIS</th>
        <th>PROVINCIA</th>
        <th>CIUDAD</th>
        <th>DIRECCION</th>
        <th>TIPO</th>
        <th>ESTADO</th>
        <th>LATITUD</th>
        <th>LONGITUD</th>
        <th>FOTO</th>
        <th>ACCIONES</th>
      </tr>
    </thead>
    <tbody >
      <?php foreach ($listadoCajeros as $cajero): ?>
        <tr class="table-warning">
          <td><?php echo $cajero->id_pc; ?></td>
          <td><?php echo $cajero->pais_pc; ?></td>
          <td><?php echo $cajero->provincia_pc; ?></td>
          <td><?php echo $cajero->ciudad_pc; ?></td>
          <td><?php echo $cajero->direccion_pc; ?></td>
          <td><?php echo $cajero->tipo_pc; ?></td>
          <td><?php echo $cajero->estado_pc; ?></td>
          <td><?php echo $cajero->latitud_pc; ?></td>
          <td><?php echo $cajero->longitud_pc; ?></td>
          <td>
            <?php if ($cajero->foto_pc!=""): ?>
              <img src="<?php echo base_url('uploads/cajeros/').$cajero->foto_pc; ?>"
              height="100px" alt="">
            <?php else: ?>
              N/A
            <?php endif; ?>
          </td>
          <td>
            <a href="<?php echo site_url('cajeros/editar/').$cajero->id_pc; ?>" class="btn btn-warning" title="Editar"> <i class="fa fa-pen"></i> </a>
            <a href="<?php echo site_url('cajeros/borrar/').$cajero->id_pc?>" class="btn btn-danger" title="Eliminar"> <i class="fa-solid fa fa-trash"></i> </a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
</div>
  <!-- Button trigger modal -->



<?php else: ?>
  <div class="alert alert-danger">
    NO SE ENCONTRO HOSPITALES REGISTRADOS
  </div>
<?php endif; ?>
