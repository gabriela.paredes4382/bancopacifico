<!-- partial -->
<div class="main-panel">
  <div class="content-wrapper">
    <div class="row">
      <div class="col-12 col-xl-6 grid-margin stretch-card">
        <div class="row w-100 flex-grow">
          <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <p class="card-title">Website Audience Metrics</p>
                <p class="text-muted">25% more traffic than previous week</p>
                <div class="row mb-3">
                  <div class="col-md-7">
                    <div class="d-flex justify-content-between traffic-status">
                      <div class="item">
                        <p class="mb-">Users</p>
                        <h5 class="font-weight-bold mb-0">93,956</h5>
                        <div class="color-border"></div>
                      </div>
                      <div class="item">
                        <p class="mb-">Bounce Rate</p>
                        <h5 class="font-weight-bold mb-0">58,605</h5>
                        <div class="color-border"></div>
                      </div>
                      <div class="item">
                        <p class="mb-">Page Views</p>
                        <h5 class="font-weight-bold mb-0">78,254</h5>
                        <div class="color-border"></div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-5">
                    <ul class="nav nav-pills nav-pills-custom justify-content-md-end" id="pills-tab-custom"
                      role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" id="pills-home-tab-custom" data-toggle="pill"
                          href="#pills-health" role="tab" aria-controls="pills-home" aria-selected="true">
                          Day
                        </a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="pills-profile-tab-custom" data-toggle="pill" href="#pills-career"
                          role="tab" aria-controls="pills-profile" aria-selected="false">
                          Week
                        </a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="pills-contact-tab-custom" data-toggle="pill" href="#pills-music"
                          role="tab" aria-controls="pills-contact" aria-selected="false">
                          Month
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
                <canvas id="audience-chart"></canvas>
              </div>
            </div>
          </div>
          <div class="col-md-6 stretch-card">
            <div class="card">
              <div class="card-body">
                <div class="d-flex align-items-center justify-content-between flex-wrap">
                  <p class="card-title">Weekly Balance</p>
                  <p class="text-success font-weight-medium">20.15 %</p>
                </div>
                <div class="d-flex align-items-center flex-wrap mb-3">
                  <h5 class="font-weight-normal mb-0 mb-md-1 mb-lg-0 mr-3">$22.736</h5>
                  <p class="text-muted mb-0">Avg Sessions</p>
                </div>
                <canvas id="balance-chart" height="130"></canvas>
              </div>
            </div>
          </div>
          <div class="col-md-6 stretch-card">
            <div class="card">
              <div class="card-body">
                <div class="d-flex align-items-center justify-content-between flex-wrap">
                  <p class="card-title">Today Task</p>
                  <p class="text-success font-weight-medium">45.39 %</p>
                </div>
                <div class="d-flex align-items-center flex-wrap mb-3">
                  <h5 class="font-weight-normal mb-0 mb-md-1 mb-lg-0 mr-3">17.247</h5>
                  <p class="text-muted mb-0">Avg Sessions</p>
                </div>
                <canvas id="task-chart" height="130"></canvas>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12 col-xl-6 grid-margin stretch-card">
        <div class="row w-100 flex-grow">
          <div class="col-md-6 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <p class="card-title">Regional Load</p>
                <p class="text-muted">Last update: 2 Hours ago</p>
                <div class="regional-chart-legend d-flex align-items-center flex-wrap mb-1"
                  id="regional-chart-legend"></div>
                <canvas height="280" id="regional-chart"></canvas>
              </div>
            </div>
          </div>
          <div class="col-md-6 grid-margin stretch-card">
            <div class="card">
              <div class="card-body pb-0">
                <div class="d-flex align-items-center mb-4">
                  <p class="card-title mb-0 mr-1">Today activity</p>
                  <div class="badge badge-info badge-pill">2</div>
                </div>
                <div class="d-flex flex-wrap pt-2">
                  <div class="mr-4 mb-lg-2 mb-xl-0">
                    <p>Time On Site</p>
                    <h4 class="font-weight-bold mb-0">77.15 %</h4>
                  </div>
                  <div>
                    <p>Page Views</p>
                    <h4 class="font-weight-bold mb-0">14.15 %</h4>
                  </div>
                </div>
              </div>
              <canvas height="150" id="activity-chart"></canvas>
            </div>
          </div>
          <div class="col-md-12 stretch-card">
            <div class="card">
              <div class="card-body pb-0">
                <p class="card-title">Server Status 247</p>
                <div class="d-flex justify-content-between flex-wrap">
                  <p class="text-muted">Last update: 2 Hours ago</p>
                  <div class="d-flex align-items-center flex-wrap server-status-legend mt-3 mb-3 mb-md-0">
                    <div class="item mr-3">
                      <div class="d-flex align-items-center">
                        <div class="color-bullet"></div>
                        <h5 class="font-weight-bold mb-0">128GB</h5>
                      </div>
                      <p class="mb-">Total Usage</p>
                    </div>
                    <div class="item mr-3">
                      <div class="d-flex align-items-center">
                        <div class="color-bullet"></div>
                        <h5 class="font-weight-bold mb-0">92%</h5>
                      </div>
                      <p class="mb-">Memory Usage</p>
                    </div>
                    <div class="item mr-3">
                      <div class="d-flex align-items-center">
                        <div class="color-bullet"></div>
                        <h5 class="font-weight-bold mb-0">16%</h5>
                      </div>
                      <p class="mb-">Disk Usage</p>
                    </div>
                  </div>
                </div>
              </div>
              <canvas height="170" id="status-chart"></canvas>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Reporte de Ubicaciones de Doctores</h4>
            <?php
            // Realiza la conexión a tu base de datos y obtén las ubicaciones
            $conexion = new mysqli("localhost", "root", "", "bancoPacifico");

            if ($conexion->connect_error) {
                die("Error de conexión a la base de datos: " . $conexion->connect_error);
            }

            // Consulta para obtener las ubicaciones de las sucursales
            $sucursalesQuery = "SELECT * FROM sucursal";
            $sucursalesResult = $conexion->query($sucursalesQuery);

            // Consulta para obtener las ubicaciones de los cajeros
            $cajerosQuery = "SELECT * FROM cajero";
            $cajerosResult = $conexion->query($cajerosQuery);

            // Consulta para obtener las ubicaciones de los corresponsales
            $corresponsalesQuery = "SELECT * FROM corresponsal";
            $corresponsalesResult = $conexion->query($corresponsalesQuery);

            // Función para obtener el listado de ubicaciones
            function obtenerListado($result)
            {
                return $result ? $result->fetch_all(MYSQLI_ASSOC) : [];
            }

            $listadoSucursales = obtenerListado($sucursalesResult);
            $listadoCajeros = obtenerListado($cajerosResult);
            $listadoCorresponsales = obtenerListado($corresponsalesResult);
            ?>

            <?php if ($listadoSucursales || $listadoCajeros || $listadoCorresponsales): ?>
                <table class="table table-bordered">
                    <!-- ... (resto del código HTML de la tabla) ... -->
                </table>

                <!-- Agrega el contenedor div para el mapa que ocupará el 80% del tamaño de la ventana -->
                <div id="reporteMapa" style="height: 80vh; width: 100%; border: 2px solid black;"></div>

                <script type="text/javascript">
                    // Inicializa el mapa después de que la API de Google Maps se haya cargado completamente
                    function initMap() {
                        var coordenadaCentral = new google.maps.LatLng(-0.9331171090790464, -78.62036815621722);
                        var miMapa = new google.maps.Map(
                            document.getElementById('reporteMapa'),
                            {
                                center: coordenadaCentral,
                                zoom: 8,
                                mapTypeId: google.maps.MapTypeId.ROADMAP
                            }
                        );

                        <?php foreach ($listadoSucursales as $sucursal): ?>
                            var coordenadaTemporal = new google.maps.LatLng(
                                <?php echo $sucursal['latitud_pc']; ?>,
                                <?php echo $sucursal['longitud_pc']; ?>
                            );
                            var marcador = new google.maps.Marker({
                                position: coordenadaTemporal,
                                map: miMapa,
                                title: '<?php echo $sucursal['nombre_pc']; ?>',
                            });
                        <?php endforeach; ?>

                        <?php foreach ($listadoCajeros as $cajero): ?>
                            var coordenadaTemporal = new google.maps.LatLng(
                                <?php echo $cajero['latitud_pc']; ?>,
                                <?php echo $cajero['longitud_pc']; ?>
                            );
                            var marcador = new google.maps.Marker({
                                position: coordenadaTemporal,
                                map: miMapa,
                                title: '<?php echo $cajero['pais_pc']; ?>',
                            });
                        <?php endforeach; ?>

                        <?php foreach ($listadoCorresponsales as $corresponsal): ?>
                            var coordenadaTemporal = new google.maps.LatLng(
                                <?php echo $corresponsal['latitud_pc']; ?>,
                                <?php echo $corresponsal['longitud_pc']; ?>
                            );
                            var marcador = new google.maps.Marker({
                                position: coordenadaTemporal,
                                map: miMapa,
                                title: '<?php echo $corresponsal['nombre_pc']; ?>',
                            });
                        <?php endforeach; ?>
                    }

                    // Carga la API de Google Maps y llama a initMap después de cargar
                    function cargarMapa() {
                        var script = document.createElement('script');
                        script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBkOn-G8apbS7PC94OKXO85lv1zayZk-9o&libraries=places&callback=initMap';
                        document.body.appendChild(script);
                    }

                    // Llama a cargarMapa cuando la ventana se ha cargado completamente
                    window.onload = cargarMapa;
                </script>

            <?php else: ?>
                <div class="alert alert-danger">
                    NO SE ENCONTRARON UBICACIONES REGISTRADAS
                </div>
            <?php endif; ?>

            <?php
            // Cierra la conexión a la base de datos
            $conexion->close();
            ?>
          </div>
        </div>
      </div>
    </div>
    <!-- row end -->
