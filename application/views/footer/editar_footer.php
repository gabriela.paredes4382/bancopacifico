<form action="<?= site_url('footer/actualizar') ?>" method="post">
    <input type="hidden" name="id_pc" id="id_pc" value="<?php echo $footerEditar->id_pc; ?>">
    <div class="form-group">
        <label for="direccion">Dirección:</label>
        <input type="text" class="form-control" id="direccion" name="direccion" value="<?= $footerEditar['direccion'] ?>">
    </div>
    <div class="form-group">
        <label for="telefono">Teléfono:</label>
        <input type="text" class="form-control" id="telefono" name="telefono" value="<?= $footerEditar['telefono'] ?>">
    </div>
    <div class="form-group">
        <label for="email">Correo Electrónico:</label>
        <input type="email" class="form-control" id="email" name="email" value="<?= $footerEditar['email'] ?>">
    </div>
    <button type="submit" class="btn btn-primary">Actualizar</button>
</form>
