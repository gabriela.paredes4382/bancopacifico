<h2>
  <b>
    <i class="fa fa-plus-circle"></i>
    NUEVO Corresponsal
  </b>
</h2>
<br>
<form class="" action="<?php echo site_url('corresponsales/guardarCorresponsal') ?>" method="post" enctype="multipart/form-data">
  <label for=""> <b>NOMBRE:</b> </label>
  <input type="text" name="nombre_pc" id="nombre_pc" value="" class="form-control" placeholder="Ingrese el nombre" required>
  <br>
  <label for=""> <b>PAIS:</b> </label>
  <input type="text" name="pais_pc" id="pais_pc" value="" class="form-control" placeholder="Ingrese el País" required>
  <br>
  <label for=""> <b>PROVINCIA:</b> </label>
  <input type="text" name="provincia_pc" id="provincia_pc" value="" class="form-control" placeholder="Ingrese la provincia" required>
  <br>
  <label for=""> <b>CIUDAD:</b> </label>
  <input type="text" name="ciudad_pc" id="ciudad_pc" value="" class="form-control" placeholder="Ingrese la ciudad" required>
  <br>
  <label for=""> <b>DIRECCIÓN:</b> </label>
  <textarea name="direccion_pc" id="direccion_pc" rows="8" cols="80" class="form-control" placeholder="Ingrese la direccion" required></textarea>
  <br>
  <label for=""> <b>TIPO:</b> </label>
  <input type="text" name="tipo_pc" id="tipo_pc" value="" class="form-control" placeholder="Ingrese el tipo" required>
  <br>
  <label for=""> <b>HORARIO DE ATENCIÓN:</b> </label>
  <input type="text" name="horario_pc" id="horario_pc" value="" class="form-control" placeholder="Ingrese el horario" required>
  <br>
  <label for=""> <b>SERVICIO:</b> </label>
  <input type="text" name="servicio_pc" id="servicio_pc" value="" class="form-control" placeholder="Ingrese el servicio" required>
  <br>
  <label for=""> <b>FOTO:</b> </label>
  <input type="file" accept="image/*" name="foto_pc" id="foto_pc" value="" class="form-control" required>
  <br>
  <div class="row">
    <div class="col-md-6">
      <label for=""> <b>LATITUD:</b> </label>
      <input type="number" name="latitud_pc" id="latitud_pc" value="" class="form-control" placeholder="Ingrese la latitud" required readonly>
    </div>
    <div class="col-md-6">
      <label for=""> <b>LONGITUD:</b> </label>
      <input type="numbre" name="longitud_pc" id="longitud_pc" value="" class="form-control" placeholder="Ingrese la longitud" required  readonly>
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col-md-12">
      <div id="mapa" style="height:300px; width:100%; border:1px solid black;">

      </div>
    </div>
  </div>
  <br>
  <!--Botones de guardar y cancelar -->
  <div class="row">
    <div class="col-md-12 text-center">
      <button type="submit" name="button"  class="btn btn-primary"><i class="fa-regular fa-floppy-disk fa-spin"></i> Guardar</button>
      <a href="<?php echo site_url("corresponsales/index") ?>" class="btn btn-danger"><i class="fa-solid fa-xmark fa-spin"></i> Cancelar</a>
    </div>
  </div>
</form>
<br><br>
<script type="text/javascript">
  function initMap(){
    var coordenadaCentral= new google.maps.LatLng(-0.15706791128777756, -78.4806844554514);
    var miMapa=new google.maps.Map(document.getElementById('mapa'),
  {
    center:coordenadaCentral,
    zoom:8,
    mapTypeId:google.maps.MapTypeId.ROADMAP
  });
  var marcador=new google.maps.Marker({
    position:coordenadaCentral,
    map:miMapa,
    title:'Selecciona la ubicacion',
    draggable:true
  });
  google.maps.event.addListener(
    marcador,
    'dragend',
    function(event){
      var latitud=this.getPosition().lat();
      var longitud=this.getPosition().lng();
      document.getElementById('latitud_pc').value=latitud;
      document.getElementById('longitud_pc').value=longitud;
    }
  );
  }
</script>
