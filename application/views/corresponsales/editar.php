<h1><i class="mdi mdi-credit-card menu-icon"></i>  Corresponsales</h1><br>
<div class="row">
  <div class="col-md-12 text-end">
    <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
      <i class="fa fa-eye"></i> VER MAPA
    </button>
    <a href="<?php echo site_url('corresponsales/nuevo') ?>" class="btn btn-outline-success"> <i class="fa fa-plus-circle"></i>  Agregar Corresponsales</a>
    <br><br>
  </div>
</div>
<?php if ($listadoCorresponsales): ?>
<div class="table-responsive pt-3">
  <table class="table table-bordered">
    <thead>
      <tr class="table-info">
        <th>ID</th>
        <th>NOMBRE</th>
        <th>PAIS</th>
        <th>PROVINCIA</th>
        <th>CIUDAD</th>
        <th>DIRECCION</th>
        <th>TIPO</th>
        <th>HORARIO</th>
        <th>SERVICIO</th>
        <th>LATITUD</th>
        <th>LONGITUD</th>
        <th>FOTO</th>
        <th>ACCIONES</th>
      </tr>
    </thead>
    <tbody >
      <?php foreach ($listadoCorresponsales as $corresponsal): ?>
        <tr class="table-warning">
          <td><?php echo $corresponsal->id_pc; ?></td>
          <td><?php echo $corresponsal->nombre_pc; ?></td>
          <td><?php echo $corresponsal->pais_pc; ?></td>
          <td><?php echo $corresponsal->provincia_pc; ?></td>
          <td><?php echo $corresponsal->ciudad_pc; ?></td>
          <td><?php echo $corresponsal->direccion_pc; ?></td>
          <td><?php echo $corresponsal->tipo_pc; ?></td>
          <td><?php echo $corresponsal->horario_pc; ?></td>
          <td><?php echo $corresponsal->servicio_pc; ?></td>
          <td><?php echo $corresponsal->latitud_pc; ?></td>
          <td><?php echo $corresponsal->longitud_pc; ?></td>
          <td>
            <?php if ($corresponsal->fotografia_pc!=""): ?>
              <img src="<?php echo base_url('uploads/corresponsales/').$corresponsal->fotografia_pc; ?>"
              height="100px" alt="">
            <?php else: ?>
              N/A
            <?php endif; ?>
          </td>
          <td>
            <a href="<?php echo site_url('corresponsales/editar/').$corresponsal->id_pc; ?>" class="btn btn-warning" title="Editar"> <i class="fa fa-pen"></i> </a>
            <a href="<?php echo site_url('corresponsales/borrar/').$corresponsal->id_pc?>" class="btn btn-danger" title="Eliminar"> <i class="fa-solid fa fa-trash"></i> </a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
</div>
  <!-- Button trigger modal -->

  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel"> <i class="fa fa-eye"></i> Mapa de Cajeros Registrados</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div id="reporteMapa" style="height:300px; width:100%; border:2px solid black"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>

  <script>
    function initMap() {
      var coordenadaCentral = new google.maps.LatLng(-0.9331171090790464, -78.62036815621722);
      var miMapa = new google.maps.Map(
        document.getElementById('reporteMapa'), {
          center: coordenadaCentral,
          zoom: 8,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
      );
      <?php foreach ($listadoCorresponsales as $corresponsal): ?>
      var coordenadaTemporal = new google.maps.LatLng(
        <?php echo $corresponsal->latitud_pc; ?>,
        <?php echo $corresponsal->longitud_pc; ?>
      );
      var marcador = new google.maps.Marker({
        position: coordenadaTemporal,
        map: miMapa,
        title: '<?php echo $corresponsal->nombre_pc; ?>'
      });
      <?php endforeach; ?>
    }
  </script>


<?php else: ?>
  <div class="alert alert-danger">
    NO SE ENCONTRO CORRESPONSALES REGISTRADOS
  </div>
<?php endif; ?>
