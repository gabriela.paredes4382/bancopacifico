<footer class="footer">
  <div class="card">
    <div class="card-body">
      <div class="container-fluid">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <div class="dummy_text"><img src="<?= base_url('assets/images1/map-icon.png') ?>"><h5 class="address_text">Address : <?= isset($footer['direccion']) ? $footer['direccion'] : '' ?></h5></div>
            <div class="call_text"><img src="<?= base_url('assets/images1/call-icon.png') ?>"><h5 class="address_text">Phone :  <?= isset($footer['telefono']) ? $footer['telefono'] : '' ?></h5></div>
            <div class="call_text"><img src="<?= base_url('assets/images1/email-icon.png') ?>"><h5 class="address_text">Email : <?= isset($footer['email']) ? $footer['email'] : '' ?></h5></div>
            <a href="<?= site_url('footer/editar') ?>" class="btn btn-primary">Editar</a>
          </div>
      </div>
    </div>
  </div>
</footer>



<!-- partial -->
</div>
<!-- main-panel ends -->
</div>
<!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->

<!-- base:js -->
<script src="<?= base_url('assets/vendors/js/vendor.bundle.base.js') ?>"></script>
<!-- Plugin js for this page-->
<script src="<?= base_url('assets/vendors/chart.js/Chart.min.js') ?>"></script>
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="<?= base_url('assets/js/off-canvas.js') ?>"></script>
<script src="<?= base_url('assets/js/hoverable-collapse.js') ?>"></script>
<script src="<?= base_url('assets/js/template.js') ?>"></script>
<!-- endinject -->
<!-- plugin js for this page -->
<!-- End plugin js for this page -->
<!-- Custom js for this page-->
<script src="<?= base_url('assets/js/dashboard.js') ?>"></script>
<!-- End custom js for this page-->
</body>

</html>
